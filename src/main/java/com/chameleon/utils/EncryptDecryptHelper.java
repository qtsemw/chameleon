package com.chameleon.utils;

import java.security.Security;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import com.chameleon.AutomationException;
import com.chameleon.ChameleonGlobalProperties;

public class EncryptDecryptHelper {
    private static StandardPBEStringEncryptor encryptor;

    private synchronized static StandardPBEStringEncryptor getInstance(final String password) {
        if (encryptor == null) {
            Security.addProvider(new BouncyCastleProvider());

            encryptor = new StandardPBEStringEncryptor();
            encryptor.setProviderName("BC");
            encryptor.setAlgorithm("PBEWITHSHA256AND256BITAES-CBC-BC");
            encryptor.setPassword(password);
            encryptor.initialize();
        }

        return encryptor;
    }

    public synchronized static String encrypt(final String value, final String password) {
        if (StringUtils.isEmpty(value)) {
            throw new AutomationException("Value to encrypt cannot be null");
        }

        if (StringUtils.isEmpty(password)) {
            throw new AutomationException("Password cannot be null");
        }

        return getInstance(password).encrypt(value);
    }

    public synchronized static String encrypt(final String value) {
        return encrypt(value, ChameleonGlobalProperties.getDefaultEncryptionPassword());
    }

    public synchronized static String decrypt(final String encrypteddata, final String password) {
        String decryptedValue = null;
        if (StringUtils.isNotEmpty(encrypteddata)) {
            if (StringUtils.isEmpty(password)) {
                throw new AutomationException("Password cannot be null");
            }

            decryptedValue = getInstance(password).decrypt(encrypteddata);
            if (StringUtils.isEmpty(decryptedValue)) {
                return null;
            }
        }
        return decryptedValue;
    }

    public synchronized static String decrypt(final String encrypteddata) {
        return decrypt(encrypteddata, ChameleonGlobalProperties.getDefaultEncryptionPassword());
    }

}