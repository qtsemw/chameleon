package com.chameleon.utils.cucumber.objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Tag {

    @JsonProperty("line")
    private Long line;
    @JsonProperty("name")
    private String name;

    @JsonProperty("line")
    public Long getLine() {
        return line;
    }

    @JsonProperty("line")
    public void setLine(Long line) {
        this.line = line;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

}