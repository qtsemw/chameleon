package com.chameleon.utils.cucumber;

import static com.chameleon.utils.cucumber.CucumberConstants.REPORT_OUTPUT_LOCATION;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.chameleon.AutomationException;
import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.DriverManagerFactory;
import com.chameleon.selenium.DriverProperties;
import com.chameleon.selenium.DriverType;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.utils.Constants;
import com.chameleon.utils.date.DateTimeConversion;
import com.chameleon.utils.io.PomProperties;

import cucumber.api.Scenario;

/**
 * The expectation is that this class contain ability to start and stop webdriver in cucumber scripts
 *
 * @author Justin Phlegar
 *
 */

public class WebBaseSteps {

    public ExtendedWebDriver getWebDriver() {
        return DriverManager.getWebDriver();
    }

    public void setup() {
        // Check if running via Maven. If not, use default browser defined by constants
        final String browser = StringUtils.isEmpty(PomProperties.getProperty("browser")) ? DriverProperties.getDefaultBrowser() : PomProperties.getProperty("browser");
        DriverManagerFactory.getManager(DriverType.fromString(browser)).initalizeDriver();
    }

    public void teardown(final Scenario scenario) {
        // Screenshot on failure:
        if (scenario.isFailed()) {

            try {
                final File src = ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.FILE);

                // Get the User Story tag for the User Story
                String userStoryTag = "";
                String testCaseTag = "";

                // Retrieve the User Story and Test Case tags (cleaned of @).
                for (final String tag : scenario.getSourceTagNames()) {
                    if (tag.contains(CucumberConstants.FEATURE_TAG)) {
                        userStoryTag = tag.replace("@", "").trim();
                    } else if (tag.contains(CucumberConstants.SCENARIO_TAG)) {
                        testCaseTag = tag.replace("@", "").trim();
                    }
                }

                // Generate timestamp for screenshot file name.
                final String fileTimeStamp = DateTimeConversion.getDaysOut("0", "MMddyyyy-HHmmssSSS");

                // Build the name of the file with all components.
                final String fileName = userStoryTag + "-" + testCaseTag + "--" + fileTimeStamp;

                // Build the filepath to write the file and write the file.
                final String path = REPORT_OUTPUT_LOCATION + userStoryTag + Constants.DIR_SEPARATOR + testCaseTag + Constants.DIR_SEPARATOR + fileName + ".png";
                FileUtils.copyFile(src, new File(path));

                // Write the screenshot filepath/file to the gherkin JSON.
                scenario.write(path);

            } catch (Exception e) {
                throw new AutomationException("Failed to take screenshot", e);
            }
        }

        DriverManager.quitDriver();
    }

}