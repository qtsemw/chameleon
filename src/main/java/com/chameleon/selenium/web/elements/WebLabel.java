package com.chameleon.selenium.web.elements;

import org.openqa.selenium.By;

import com.chameleon.selenium.elements.Label;
import com.chameleon.selenium.web.ExtendedWebDriver;

/**
 * Wraps a label on a html form with some behavior.
 */
public class WebLabel extends Label {
    /**
     * Creates an Element for a given WebElement.
     *
     * @param extendedElement
     *            element to wrap up
     */

    public WebLabel(ExtendedWebDriver driver, By by) {
        super(driver, by);
    }

    public WebLabel(ExtendedWebDriver driver, By by, org.openqa.selenium.WebElement element) {
        super(driver, by, element);
    }

}