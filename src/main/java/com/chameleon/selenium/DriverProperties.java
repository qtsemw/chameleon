package com.chameleon.selenium;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.chameleon.utils.io.PropertiesManager;

public class DriverProperties {
    private static List<String> validProperties = Arrays.asList(
            "chameleon.selenium.default.browser",
            "chameleon.selenium.hub.mobile.url",
            "chameleon.selenium.hub.mobile.key",
            "chameleon.selenium.hub.saucelabs.username",
            "chameleon.selenium.hub.saucelabs.key",
            "chameleon.selenium.hub.web.url",
            "chameleon.selenium.timeout.driver",
            "chameleon.selenium.timeout.element",
            "chameleon.selenium.timeout.page",
            "chameleon.selenium.timeout.polling");

    private static ThreadLocal<Map<String, String>> seleniumProperties = ThreadLocal.withInitial(() -> {
        Map<String, String> map = new HashMap<>();
        return map;
    });

    public DriverProperties(final Map<String, String> props) {
        initialize(props);
    }

    public static synchronized void initialize(final Map<String, String> props) {
        validProperties.stream()
                .filter(propertyKey -> props.containsKey(propertyKey))
                .forEach(propertyKey -> seleniumProperties.get().put(propertyKey, props.get(propertyKey)));
    }

    public static int getDriverTimeout() {
        final String level = get("chameleon.selenium.timeout.driver");
        return StringUtils.isEmpty(level) ? 30 : Integer.parseInt(level);
    }

    public static int getElementTimeout() {
        final String level = get("chameleon.selenium.timeout.element");
        return StringUtils.isEmpty(level) ? 5 : Integer.parseInt(level);
    }

    public static int getPageTimeout() {
        final String level = get("chameleon.selenium.timeout.page");
        return StringUtils.isEmpty(level) ? 10 : Integer.parseInt(level);
    }

    public static int getPollingTimeout() {
        final String level = get("chameleon.selenium.timeout.polling");
        return StringUtils.isEmpty(level) ? 250 : Integer.parseInt(level);
    }

    public static String getDefaultBrowser() {
        final String browser = get("chameleon.selenium.default.browser");
        return StringUtils.isEmpty(browser) ? "chrome" : browser;
    }

    public static String getMobileHubUrl() {
        return get("chameleon.selenium.hub.mobile.url");
    }

    public static String getMobileKey() {
        return get("chameleon.selenium.hub.mobile.key");
    }

    public static String getSaucelabsUsername() {
        return get("chameleon.selenium.hub.saucelabs.username");
    }

    public static String getWebHubUrl() {
        return get("chameleon.selenium.hub.web.url");
    }

    public static String getSaucelabsKey() {
        return get("chameleon.selenium.hub.saucelabs.key");
    }

    private static synchronized String get(final String property) {
        if (seleniumProperties.get().isEmpty()) {
            initialize(PropertiesManager.properties("config/global.properties"));
        }
        return seleniumProperties.get().get(property);
    }
}
