package com.chameleon;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.chameleon.utils.io.PropertiesManager;

public class ChameleonGlobalProperties {
    private static List<String> validProperties = Arrays.asList(
            "chameleon.logging.level",
            "chameleon.logging.print",
            "chameleon.security.encr.dcry.password",
            "chameleon.timeout.maxsleep");

    private static ThreadLocal<Map<String, String>> chameleonProperties = ThreadLocal.withInitial(() -> {
        Map<String, String> map = new HashMap<>();
        return map;
    });

    public ChameleonGlobalProperties(final Map<String, String> props) {
        initialize(props);
    }

    public static synchronized void initialize(final Map<String, String> props) {
        validProperties.stream()
                .filter(propertyKey -> props.containsKey(propertyKey))
                .forEach(propertyKey -> chameleonProperties.get().put(propertyKey, props.get(propertyKey)));
    }

    public static int getDefaultLoggingLevel() {
        final String level = get("chameleon.logging.level");
        return StringUtils.isEmpty(level) ? 0 : Integer.parseInt(level);
    }

    public static boolean getDefaultPrintToConsole() {
        final String print = get("chameleon.logging.print");
        return StringUtils.isEmpty(print) ? false : Boolean.parseBoolean(print);
    }

    public static String getDefaultEncryptionPassword() {
        final String pw = get("chameleon.security.encr.dcry.password");
        return StringUtils.isEmpty(pw) ? "automation" : pw;
    }

    public static int getDefaultMaxSleep() {
        final String maxsleep = get("chameleon.timeout.maxsleep");
        return StringUtils.isEmpty(maxsleep) ? 0 : Integer.parseInt(maxsleep);
    }

    private static synchronized String get(final String property) {
        if (chameleonProperties.get().isEmpty()) {
            initialize(PropertiesManager.properties("config/global.properties"));
        }
        return chameleonProperties.get().get(property);
    }
}
