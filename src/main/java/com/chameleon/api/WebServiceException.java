package com.chameleon.api;

import com.chameleon.AutomationException;

public class WebServiceException extends AutomationException {
    private static final long serialVersionUID = -8710980695994382082L;

    public WebServiceException(String message, Object... args) {
        super(message, args);
    }

    public WebServiceException(String message, Throwable cause, Object... args) {
        super(message, cause, args);
    }

}
