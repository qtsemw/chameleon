package com.demo.salesforce.api.rest.account;

import com.chameleon.api.restServices.RestResponse;
import com.demo.salesforce.api.rest.SalesforceRest;
import com.demo.salesforce.api.rest.account.request.AccountRequest;
import com.demo.salesforce.api.rest.response.SFResponse;

public class AccountProcessor extends SalesforceRest {
    private String resource = "Account/";
    
    public SFResponse createAccount(AccountRequest account) {
    	RestResponse createResponse = sendPostRequest(resource, account);
        return createResponse.mapJSONToObject(SFResponse.class);
    }

    public RestResponse getAccount(AccountRequest account) {
        return getAccount(account.getId());
    }

    public RestResponse getAccount(String id) {
        return sendGetRequest(resource + id);
    }

    public RestResponse deleteAccount(AccountRequest account) {
        return deleteAccount(account.getId());
    }

    public RestResponse deleteAccount(String id) {
        return sendDeleteRequest(resource + id);
    }
}
