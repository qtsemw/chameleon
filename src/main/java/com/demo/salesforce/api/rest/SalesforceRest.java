package com.demo.salesforce.api.rest;

import static com.chameleon.api.restServices.Headers.AUTH;
import static com.chameleon.api.restServices.Headers.JSON;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.chameleon.api.restServices.ResponseCodes;
import com.chameleon.api.restServices.RestResponse;
import com.chameleon.api.restServices.RestService;
import com.chameleon.utils.TestReporter;
import com.chameleon.utils.io.JsonObjectMapper;

public class SalesforceRest {

    private static final String BASE_URL = "https://na59.salesforce.com/services/data/v42.0/sobjects/";
    private RestService rest = new RestService();
    private static AuthZToken token;

    protected RestResponse sendDeleteRequest(String resource) {
        authorize();
        return rest.sendDeleteRequest(BASE_URL + resource, JSON());
    }

    protected RestResponse sendGetRequest(String resource) {
        authorize();
        return rest.sendGetRequest(BASE_URL + resource, JSON());
    }

    protected RestResponse sendPostRequest(String resource, Object o) {
        authorize();
        return rest.sendPostRequest(BASE_URL + resource, JSON(), JsonObjectMapper.getJsonFromObject(o));
    }

    protected RestResponse sendPatchRequest(String resource, Object o) {
        authorize();
        return rest.sendPatchRequest(BASE_URL + resource, JSON(), JsonObjectMapper.getJsonFromObject(o));
    }

    private void authorize() {
        if (token == null) {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("grant_type", "password"));
            params.add(new BasicNameValuePair("client_id", "3MVG9zlTNB8o8BA3gCW_61YbBj5YZm.0eHNfFrxZOJ8QOVWRXzvdJezJy0IqXW4B_E3obkBPW5KE5.lKEVKuo"));
            params.add(new BasicNameValuePair("client_secret", "7332830631411820973"));
            params.add(new BasicNameValuePair("username", "Justin.phlegar@orasi.com"));
            params.add(new BasicNameValuePair("password", "roottoor85!DahBpMpgBccfbEsYtxw1Xt1cP"));
            RestService rest = new RestService();
            RestResponse response = rest.sendPostRequest("https://login.salesforce.com/services/oauth2/token", AUTH(), params);
            token = response.mapJSONToObject(AuthZToken.class);
            TestReporter.assertEquals(response.getStatusCode(), ResponseCodes.OK, "REST API :: Successful login to Salesforce Sandbox via Rest API. "
                    + "Response Status Code: [ " + response.getStatusCode() + " ].");
        }
        rest.addCustomHeaders("Authorization", token.getTokenType() + " " + token.getAccessToken());
    }

}
