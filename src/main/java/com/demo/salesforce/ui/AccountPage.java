package com.demo.salesforce.ui;

import org.openqa.selenium.By;

public class AccountPage extends BasePage {

    /** By Locator Elements **/
    By lblTopName = By.xpath("//h2[@class='topName']");

    /** Constructor **/
    public AccountPage() {

    }

    public boolean verifyAccountPageIsDisplayed(String fullName) {
        return driver.findLabel(lblTopName).syncTextInElement(fullName);
    }

}
