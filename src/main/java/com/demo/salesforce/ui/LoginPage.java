package com.demo.salesforce.ui;

import java.util.Map;
import java.util.ResourceBundle;

import org.openqa.selenium.By;

import com.chameleon.selenium.web.WebPageLoaded;
import com.chameleon.utils.io.PropertiesManager;

public class LoginPage extends BasePage {

    public ResourceBundle userCredentialRepo;

    /** By Locator Elements **/
    By txtUserName = By.id("username");
    By txtPassword = By.id("password");
    By btnLogin = By.id("Login");
    By lnkHome = By.xpath("//a[.='Home']");
    By btnSetup = By.id("setupLink");

    // Constructor for the page.
    public LoginPage() {

    }

    /**
     * Logs into Salesforce with specified user credentials.
     *
     * @author John Martin
     * @date 03/15/2018
     * @param userName
     * @param password
     * @return boolean
     */
    public boolean validLogin() {
        Map<String, String> props = PropertiesManager.properties("config/salesforce/salesforce.properties");
        String username = props.get("app.username");
        String password = (props.get("app.password"));
        // Sync from navigation, set the username and
        // password fields and click the login button.
        WebPageLoaded.isDomComplete(10);
        driver.findTextbox(txtUserName).syncVisible(10);
        driver.findTextbox(txtUserName).setSecure(username);
        driver.findTextbox(txtPassword).setSecure(password);
        driver.findButton(btnLogin).click();

        // Sync and return a verification of a successful login.
        WebPageLoaded.isDomComplete(10);
        return driver.findButton(btnSetup).syncVisible(10);
    }

    /**
     * Logs into Salesforce with specified user credentials.
     *
     * @author John Martin
     * @date 03/15/2018
     * @param userName
     * @param password
     * @return boolean
     */
    public boolean validLogin(String userName, String password) {
        // Sync from navigation, set the username and
        // password fields and click the login button.
        WebPageLoaded.isDomComplete(10);
        driver.findTextbox(txtUserName).syncVisible(10);
        driver.findTextbox(txtUserName).set(userName);
        driver.findTextbox(txtPassword).setSecure(password);
        driver.findButton(btnLogin).click();

        // Sync and return a verification of a successful login.
        WebPageLoaded.isDomComplete(10);
        return driver.findButton(btnSetup).syncVisible(10);
    }

}
