package com.demo.windows.calculator;

import org.openqa.selenium.By;

import com.chameleon.selenium.windows.by.ByWindows;
import com.chameleon.selenium.windows.elements.WindowsLabel;
import com.chameleon.selenium.windows.elements.WindowsListbox;
import com.demo.windows.BaseUIPage;

public class CalculatorPage extends BaseUIPage {

    private ByWindows plusButton = ByWindows.accessibilityId("plusButton");
    private ByWindows minusButton = ByWindows.accessibilityId("minusButton");
    private ByWindows multiplyButton = ByWindows.accessibilityId("multiplyButton");
    private ByWindows divideButton = ByWindows.accessibilityId("divideButton");
    private ByWindows equalButton = ByWindows.accessibilityId("equalButton");
    private ByWindows results = ByWindows.accessibilityId("CalculatorResults");

    private ByWindows openMenuHeader = ByWindows.accessibilityId("Header");
    private ByWindows openMenuButton = ByWindows.accessibilityId("TogglePaneButton");
    private ByWindows menuTable = ByWindows.accessibilityId("PaneRoot");

    private ByWindows dateCalculationOption = ByWindows.accessibilityId("DateCalculationOption");
    private ByWindows fromDateLabel = ByWindows.accessibilityId("DateText");
    private ByWindows calendarView = ByWindows.accessibilityId("MonthViewScrollViewer");

    public void add(String primary, String secondary) {
        calculate(plusButton, primary, secondary);
    }

    public void add(String... numbers) {
        calculate(plusButton, numbers);
    }

    public void subtract(String primary, String secondary) {
        calculate(minusButton, primary, secondary);
    }

    public void subtract(String... numbers) {
        calculate(minusButton, numbers);
    }

    public void multiply(String primary, String secondary) {
        calculate(multiplyButton, primary, secondary);
    }

    public void multiply(String... numbers) {
        calculate(multiplyButton, numbers);
    }

    public void divide(String primary, String secondary) {
        calculate(divideButton, primary, secondary);
    }

    public void divide(String... numbers) {
        calculate(divideButton, numbers);
    }

    public String getResult() {
        return getDriver().findTextbox(results).getText().replace("Display is", "").trim();
    }

    public void selectMenuOption(String option) {
        getDriver().findButton(openMenuButton).click();
        getDriver().findButton(menuTable).syncVisible();
        WindowsLabel lblOption = getDriver().findLabel(By.name(option + " Calculator"));
        lblOption.syncVisible();
        lblOption.click();
    }

    public void selectMenuOptionIfNotLoaded(String option) {
        if (!getDriver().findLabel(openMenuHeader).getText().contains(option)) {
            getDriver().findButton(openMenuButton).click();
            getDriver().findButton(menuTable).syncVisible();
            WindowsLabel lblOption = getDriver().findLabel(By.name(option + " Calculator"));
            lblOption.syncVisible();
            lblOption.click();
        }
    }

    public void differenceBetweenDates() {
        WindowsListbox list = getDriver().findListbox(dateCalculationOption);
        if (!list.syncTextInElement("Difference between dates", 3, false)) {
            list.click();
            list.select("Difference between dates");
        }
        getDriver().findLabel(fromDateLabel).click();

    }

    private void calculate(ByWindows mathButton, String primary, String secondary) {
        getDriver().findButton(ByWindows.accessibilityId("num" + primary + "Button")).click();
        getDriver().findButton(mathButton).click();
        getDriver().findButton(ByWindows.accessibilityId("num" + secondary + "Button")).click();
        getDriver().findButton(equalButton).click();
    }

    private void calculate(ByWindows mathButton, String... numbers) {
        // Handle initial number
        getDriver().findButton(ByWindows.accessibilityId("num" + numbers[0] + "Button")).click();
        for (int x = 1; x < numbers.length; x++) {
            // Remaining numbers iterate after click action button
            getDriver().findButton(mathButton).click();
            getDriver().findButton(ByWindows.accessibilityId("num" + numbers[x] + "Button")).click();
        }
        getDriver().findButton(equalButton).click();
    }
}
