package com.demo.windows.dragDrop;

import java.util.List;

import org.openqa.selenium.By;

import com.chameleon.selenium.windows.by.ByWindows;
import com.chameleon.selenium.windows.elements.WindowsButton;
import com.chameleon.selenium.windows.elements.WindowsListbox;
import com.chameleon.selenium.windows.elements.WindowsTextbox;
import com.demo.windows.BaseUIPage;

public class DragDropPage extends BaseUIPage {

    public final String TITLE_TEXT = "TITLE_TEXT";
    private ByWindows txtTitleEditbox = ByWindows.accessibilityId("textBox1");
    private By txtSpinnerEditbox = By.xpath("//ComboBox[starts-with(@AutomationId,\"numericUpDown\")]/Edit");
    private By btnSpinnerUp = By.name("Up");
    private By btnSpinnerDown = By.name("Down");
    private ByWindows txtDateEditbox = ByWindows.accessibilityId("dateTimePicker1");
    private ByWindows txtRichTextbox = ByWindows.accessibilityId("richTextBox1");
    private ByWindows lstDropList = ByWindows.accessibilityId("listBox1");

    public String getTitleBoxText() {
        return getDriver().findTextbox(txtTitleEditbox).getText();
    }

    public void dragTitleBoxText() {
        final WindowsTextbox title = getDriver().findTextbox(txtTitleEditbox);
        final WindowsListbox dropList = getDriver().findListbox(lstDropList);

        title.syncEnabled();
        dropList.syncEnabled();

        getDriver().actions().dragAndDrop(title, dropList).build().perform();
    }

    public String getSpinnerText() {
        return getDriver().findTextbox(txtSpinnerEditbox).getText();
    }

    public void incrementSpinner() {
        final WindowsButton spinUp = getDriver().findButton(btnSpinnerUp);
        spinUp.syncEnabled();
        spinUp.click();
    }

    public void decrementSpinner() {
        final WindowsButton spinDown = getDriver().findButton(btnSpinnerDown);
        spinDown.syncEnabled();
        spinDown.click();
    }

    public void dragSpinnerText() {
        final WindowsTextbox spinner = getDriver().findTextbox(txtSpinnerEditbox);
        final WindowsListbox dropList = getDriver().findListbox(lstDropList);

        spinner.syncEnabled();
        dropList.syncEnabled();

        getDriver().actions().dragAndDrop(spinner, dropList).build().perform();
    }

    public boolean validateTextInDropList(final String text) {
        final List<String> values = getDriver().findListbox(lstDropList).getOptionTextValues();
        return values.parallelStream().anyMatch(value -> value.equals(text));
    }
}
