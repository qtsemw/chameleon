package com.demo;

public class DemoConstants {
    public final static String SOAP_PROPERTIES = "config/trainingServerSoap/trainingServerSoap.properties";
    public final static String SOAP_PROPERTIES_WITH_ENV = "config/trainingServerSoap/trainingServerSoap-%s.properties";
    public final static String REST_PROPERTIES = "config/trainingServerRest/trainingServerRest.properties";
    public final static String REST_PROPERTIES_WITH_ENV = "config/trainingServerRest/trainingServerRest-%s.properties";
    public final static String SOAP_XML_PATH = "xmls/soap";
}
