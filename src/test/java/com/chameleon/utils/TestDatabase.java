package com.chameleon.utils;

import org.testng.annotations.Test;

import com.chameleon.BaseTest;
import com.chameleon.database.Database;
import com.chameleon.database.Recordset;
import com.chameleon.database.databaseImpl.MariaDBDatabase;
import com.chameleon.database.databaseImpl.SQLiteDatabase;
import com.chameleon.utils.io.FileLoader;

public class TestDatabase extends BaseTest {
    @Test
    public void sqlLiteDb() {
        TestReporter.setDebugLevel(3);

        Database sqlLiteDb = new SQLiteDatabase(FileLoader.getAbsolutePathForResource("db/SampleDB.db"));
        sqlLiteDb.setReturnAsString(false);
        Recordset rsSqlLite = new Recordset(sqlLiteDb.getResultSet("SELECT * FROM Customers"));
        rsSqlLite.print();
        for (rsSqlLite.moveFirst(); rsSqlLite.hasNext(); rsSqlLite.moveNext()) {
            System.out.println(rsSqlLite.getInt("CustomerID") + "\t" +
                    rsSqlLite.getString("CompanyName") + "\t" +
                    rsSqlLite.getString("ContactName") + "\t" +
                    rsSqlLite.getString("ContactTitle") + "\t" +
                    rsSqlLite.getString("Address") + "\t" +
                    rsSqlLite.getString("City") + "\t" +
                    rsSqlLite.getString("State") + "\t");
        }
    }

    @Test
    public void mariaDb() {
        Database db = new MariaDBDatabase("127.0.0.1", "3306", "Sakila");
        db.setDbUserName("root");
        db.setDbPassword("toor");
        db.setReturnAsString(false); // optional, but safer. Cast during db retrieval
        Recordset rs = new Recordset(db.getResultSet("SELECT * FROM FILM"));
        rs.print();
        System.out.println(rs.getInt("film_id"));
        System.out.println(rs.getInt("rental_duration"));
        System.out.println(rs.getBigDecimal("rental_rate"));
        System.out.println(rs.getTimestamp("last_update"));
        System.out.println(rs.getString("special_features"));
    }
}
