package com.chameleon.utils.validators;

import static com.chameleon.api.restServices.Headers.AUTH;
import static com.chameleon.api.restServices.Headers.JSON;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.BaseTest;
import com.chameleon.api.restServices.RestResponse;
import com.chameleon.api.restServices.RestService;
import com.chameleon.utils.dataHelpers.personFactory.Person;
import com.chameleon.utils.io.JsonObjectMapper;
import com.chameleon.validators.JsonObjectComparator;
import com.demo.salesforce.api.rest.AuthZToken;
import com.demo.salesforce.api.rest.account.request.AccountRequest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestJsonObjectComparator extends BaseTest {
    private static AuthZToken token = null;
    private static final String BASE_URL = "https://na59.salesforce.com/services/data/v42.0/sobjects/Account/";
    private static RestService rest = new RestService();

    @BeforeClass(alwaysRun = true)
    public void getToken() {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("grant_type", "password"));
        params.add(new BasicNameValuePair("client_id", "3MVG9zlTNB8o8BA3gCW_61YbBj5YZm.0eHNfFrxZOJ8QOVWRXzvdJezJy0IqXW4B_E3obkBPW5KE5.lKEVKuo"));
        params.add(new BasicNameValuePair("client_secret", "7332830631411820973"));
        params.add(new BasicNameValuePair("username", "Justin.phlegar@orasi.com"));
        params.add(new BasicNameValuePair("password", "roottoor85!DahBpMpgBccfbEsYtxw1Xt1cP"));
        RestResponse response = rest.sendPostRequest("https://login.salesforce.com/services/oauth2/token", AUTH(), params);
        token = response.mapJSONToObject(AuthZToken.class);
        rest.addCustomHeaders("Authorization", token.getTokenType() + " " + token.getAccessToken());
    }

    @Feature("Utilities")
    @Story("Validators")
    @Test(groups = { "regression", "utils", "JsonObjectComparator" })
    public void testJsonObjectComparatorExpectPass() {
        Person person = new Person();
        AccountRequest account1 = new AccountRequest();
        account1.setName(person.getFullName());
        account1.setDescription("This account was created via REST automation");
        account1.setPhone(person.getAllPhones().get(0).getFormattedNumber());
        RestResponse response1 = rest.sendPostRequest(BASE_URL, JSON(), JsonObjectMapper.getJsonFromObject(account1));

        JsonObjectComparator compare = new JsonObjectComparator();
        compare.validate(response1, response1);
    }

    @Feature("Utilities")
    @Story("Validators")
    @Test(groups = { "regression", "utils", "JsonObjectComparator" }, expectedExceptions = AssertionError.class)
    public void testJsonObjectComparatorExpectFail() {
        Person person = new Person();
        AccountRequest account1 = new AccountRequest();
        account1.setName(person.getFullName());
        account1.setDescription("This account was created via REST automation");
        account1.setPhone(person.getAllPhones().get(0).getFormattedNumber());
        RestResponse response1 = rest.sendPostRequest(BASE_URL, JSON(), JsonObjectMapper.getJsonFromObject(account1));

        AccountRequest account2 = new AccountRequest();
        account2.setName(person.getFullName());
        account2.setDescription("This account was created via REST automation");
        account1.setPhone(person.getAllPhones().get(0).getFormattedNumber());

        RestResponse response2 = rest.sendPostRequest(BASE_URL, JSON(), JsonObjectMapper.getJsonFromObject(account2));

        JsonObjectComparator compare = new JsonObjectComparator();
        compare.validate(response1, response2);
    }

}
