package com.chameleon.selenium.web.webelements;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.elements.Checkbox;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.elements.WebCheckbox;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestCheckbox extends WebBaseTest {
    private ExtendedWebDriver driver;

    @BeforeClass(groups = { "regression", "interfaces", "checkbox", "dev" })
    public void setup() {
        setApplicationUnderTest("Test Site");
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/checkbox.html");
    }

    @BeforeMethod(alwaysRun = true)
    public void setDriver() {
        DriverManager.setDriver(driver);
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Element Interfaces")
    @Story("Checkbox")

    @Test(groups = { "regression", "interfaces", "checkbox" })
    public void isChecked() {
        driver = testStart("TestCheckbox");
        Checkbox checkbox = driver.findCheckbox(By.name("checkbox"));
        Assert.assertFalse(checkbox.isChecked());
    }

    @Feature("Element Interfaces")
    @Story("Checkbox")

    @Test(groups = { "regression", "interfaces", "checkbox" }, dependsOnMethods = "isChecked")
    public void check() {
        Checkbox checkbox = driver.findCheckbox(By.name("checkbox"));
        checkbox.check();
        Assert.assertTrue(checkbox.isChecked());
    }

    @Feature("Element Interfaces")
    @Story("Checkbox")

    @Test(groups = { "regression", "interfaces", "checkbox" }, dependsOnMethods = "jsToggle")
    public void toggle() {
        Checkbox checkbox = driver.findCheckbox(By.name("checkbox"));
        checkbox.toggle();
        Assert.assertFalse(checkbox.isChecked());
    }

    @Feature("Element Interfaces")
    @Story("Checkbox")

    @Test(groups = { "regression", "interfaces", "checkbox" }, dependsOnMethods = "check")
    public void uncheck() {
        Checkbox checkbox = driver.findCheckbox(By.name("checkbox"));
        checkbox.uncheck();
        Assert.assertFalse(checkbox.isChecked());
    }

    @Feature("Element Interfaces")
    @Story("Checkbox")

    @Test(groups = { "regression", "interfaces", "checkbox" }, dependsOnMethods = "uncheck")
    public void jsToggle() {
        WebCheckbox checkbox = driver.findCheckbox(By.name("checkbox"));
        checkbox.jsToggle();
        Assert.assertTrue(checkbox.isChecked());
    }
}
