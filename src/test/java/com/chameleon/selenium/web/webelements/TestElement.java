package com.chameleon.selenium.web.webelements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.DriverType;
import com.chameleon.selenium.elements.Element;
import com.chameleon.selenium.exceptions.ElementAttributeValueNotMatchingException;
import com.chameleon.selenium.exceptions.ElementCssValueNotMatchingException;
import com.chameleon.selenium.exceptions.ElementNotDisabledException;
import com.chameleon.selenium.exceptions.ElementNotEnabledException;
import com.chameleon.selenium.exceptions.ElementNotHiddenException;
import com.chameleon.selenium.exceptions.ElementNotVisibleException;
import com.chameleon.selenium.exceptions.TextInElementNotPresentException;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.elements.WebElement;
import com.chameleon.selenium.web.elements.WebTextbox;
import com.chameleon.utils.TestReporter;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestElement extends WebBaseTest {
    private ExtendedWebDriver driver;

    @BeforeClass(groups = { "regression", "interfaces", "element", "dev" })
    public void setup() {
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/element.html");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        DriverManager.setDriver(driver);
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" })
    public void reload() {
        TestReporter.setDebugLevel(3);
        driver = testStart("TestElement");
        driver.findElement(By.id("text1")).sendKeys("blah");
        Element element = driver.findElement(By.id("text1"));
        driver.get(getPageURL());
        driver.debug().setHighlightOnSync(true);
        Assert.assertTrue(element.isEnabled());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void clear() {
        driver.findElement(By.id("text1")).syncVisible();
        WebElement element = driver.findElement(By.id("text1"));
        element.clear();
        if (DriverManager.isMobileDriver()) {
            Assert.assertTrue(element.getText().equals(""));
        } else {
            Assert.assertTrue(element.getAttribute("value").equals(""));
        }

    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void findElement() {
        WebElement element = driver.findElement(By.id("radiogroup"));
        element.highlight();
        Assert.assertTrue(element instanceof WebElement);
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void findWebElement() {
        WebElement webElement = driver.findElement(By.id("radiogroup"));
        Assert.assertTrue(webElement instanceof WebElement);
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void findElements() {
        List<WebElement> elements = driver.findWebElement(By.id("radiogroup")).findElements(By.name("sex"));
        Assert.assertTrue(!elements.isEmpty());
        Assert.assertTrue(elements.get(0) instanceof org.openqa.selenium.WebElement);
        Assert.assertTrue(elements.size() == 2);
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void findWebElements() {
        List<WebElement> webElements = driver.findWebElements(By.xpath("//input"));
        Assert.assertTrue(webElements.get(0) instanceof WebElement);
        Assert.assertTrue(webElements.size() > 0);
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true, expectedExceptions = NoSuchElementException.class)
    public void negativeFindElement() {
        Assert.assertNull(driver.findElement(By.id("radiogroup")).findElement(By.name("foolocator123")).getWrappedElement());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true, expectedExceptions = NoSuchElementException.class)
    public void negativeFindWebElement() {
        driver.findElement(By.id("foolocator123")).elementWired();
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void negativeFindElements() {
        List<WebElement> elements = driver.findElement(By.id("radiogroup")).findElements(By.name("foolocator123"));
        Assert.assertTrue(elements.isEmpty());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void negativeFindWebElements() {
        List<WebElement> webElements = driver.findElements(By.name("foolocator123"));
        Assert.assertTrue(webElements.isEmpty());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "clear", alwaysRun = true)
    public void click() {
        DriverManager.setDriver(driver);
        WebElement element = driver.findElement(By.id("buttonForText1"));
        if (DriverType.EDGE.equals(driver.getDriverType())) {
            element.jsClick();
        } else {
            element.click();
        }
        // temporary fix for edge click
        Assert.assertTrue(driver.findElement(By.id("text1")).getAttribute("value").equals("Clicked button"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "click", alwaysRun = true)
    public void elementWired() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertTrue(element.elementWired());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void getAttribute() {
        Element element = driver.findElement(By.xpath("//input[@value='female']"));
        Assert.assertTrue(element.getAttribute("type").equals("radio"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void getCssValue() {
        Element element = driver.findElement(By.id("buttonForText1"));
        if (DriverType.HTML.equals(driver.getDriverType())) {
            Assert.assertTrue(element.getCssValue("font-family").equals("verdana"));
        } else {
            Assert.assertTrue(!element.getCssValue("font-family").isEmpty());
        }
    }

    /*
     * @Feature("Element Interfaces")
     *
     * @Story("Element")
     *
     *
     *
     * @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
     * public void getElementLocator() {
     * Element element = driver.findElement(By.id("text1"));
     * Assert.assertNotNull(element.getElementLocator());
     * }
     */
    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void getElementLocatorInfo() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertNotNull(element.getElementLocatorInfo());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void getLocation() {
        Element element = driver.findElement(By.id("text1"));
        try {
            Assert.assertTrue(element.getLocation().getX() > 0);
            Assert.assertTrue(element.getLocation().getY() > 0);
        } catch (WebDriverException wde) {
            if (DriverType.EDGE.equals(driver.getDriverType())) {
                throw new AssertionError("getLocation not supported by EdgeDriver");
            }
        }
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void getSize() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertTrue(element.getSize().getHeight() > 0);
        Assert.assertTrue(element.getSize().getWidth() > 0);
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void getTagName() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertTrue(element.getTagName().equals("input"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void getText() {
        Element element = driver.findElement(By.id("pageheader"));
        Assert.assertTrue(element.getText().equals("Element test page"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "getAttribute", alwaysRun = true)
    public void highlight() {
        WebElement element = driver.findElement(By.id("buttonForText1"));
        element.highlight();
        Assert.assertTrue(element.getAttribute("style").toLowerCase().contains("red"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void isDisplayed() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertTrue(element.isDisplayed());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void isEnabled() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertTrue(element.isEnabled());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void isSelected() {
        Element element = driver.findElement(By.xpath("//*[@id='radiogroup']/input[1]"));
        Assert.assertTrue(element.isSelected());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void isSelectedNegative() {
        Element element = driver.findElement(By.xpath("//*[@id='radiogroup']/input[2]"));
        Assert.assertFalse(element.isSelected());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "click", alwaysRun = true)
    public void jsClick() {
        DriverManager.setDriver(driver);
        driver.findElement(By.id("text1")).sendKeys("blah");
        WebElement element = driver.findElement(By.id("buttonForText1"));
        element.jsClick();
        Assert.assertTrue(driver.findElement(By.id("text1")).getAttribute("value").equals("Clicked button"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "jsClick", alwaysRun = true)
    public void sendKeys() {
        Element element = driver.findElement(By.id("text1"));
        element.sendKeys("testing");
        Assert.assertTrue(element.getAttribute("value").equals("Clicked buttontesting"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncDisabledBasic() {
        Element element = driver.findElement(By.id("disable"));
        Assert.assertTrue(element.syncDisabled());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = { "syncDisabledBasic" }, alwaysRun = true, expectedExceptions = ElementNotDisabledException.class)
    public void syncDisabledBasicNegative() {
        Element element = driver.findElement(By.id("text1"));
        element.syncDisabled();
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = { "syncDisabledBasic" }, alwaysRun = true)
    public void syncDisabledAdvanced() {
        Element element = driver.findElement(By.id("disable"));
        Assert.assertTrue(element.syncDisabled(5, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = { "syncDisabledBasic" }, alwaysRun = true)
    public void syncDisabledAdvancedNegative() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertFalse(element.syncDisabled(1, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void syncHiddenBasic() {
        Element element = driver.findElement(By.id("hidden"));
        Assert.assertTrue(element.syncHidden());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "elementWired", alwaysRun = true, expectedExceptions = ElementNotHiddenException.class)
    public void syncHiddenBasicNegative() {
        Element element = driver.findElement(By.id("text1"));
        element.syncHidden();
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void syncHiddenAdvanced() {
        Element element = driver.findElement(By.id("hidden"));
        Assert.assertTrue(element.syncHidden(5, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void syncHiddenAdvancedNegative() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertFalse(element.syncHidden(1, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void syncVisibleBasic() {
        WebTextbox element = driver.findTextbox(By.id("text1"));
        Assert.assertTrue(element.syncVisible());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "elementWired", alwaysRun = true, expectedExceptions = ElementNotVisibleException.class)
    public void syncVisibleBasicNegative() {
        Element element = driver.findElement(By.id("hidden"));
        element.syncVisible();
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void syncVisibleAdvanced() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertTrue(element.syncVisible(5, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void syncVisibleAdvancedNegative() {
        Element element = driver.findElement(By.id("hidden"));
        Assert.assertFalse(element.syncVisible(1, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void syncEnabledBasic() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertTrue(element.syncEnabled());
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true, expectedExceptions = ElementNotEnabledException.class)
    public void syncEnabledBasicNegative() {
        Element element = driver.findElement(By.id("disable"));
        element.syncEnabled();
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "elementWired", alwaysRun = true)
    public void syncEnabledAdvanced() {
        Element element = driver.findElement(By.id("text1"));
        Assert.assertTrue(element.syncEnabled(5, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncEnabledAdvancedNegative() {
        Element element = driver.findElement(By.id("disable"));
        Assert.assertFalse(element.syncEnabled(1, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncTextInElementBasic() {
        Element element = driver.findElement(By.id("pageheader"));
        Assert.assertTrue(element.syncTextInElement("Element test page"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true, expectedExceptions = TextInElementNotPresentException.class)
    public void syncTextInElementBasicNegative() {
        Element element = driver.findElement(By.id("pageheader"));
        element.syncTextInElement("Loading");
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncTextInElementAdvanced() {
        Element element = driver.findElement(By.id("pageheader"));
        Assert.assertTrue(element.syncTextInElement("Element test page", 5, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncTextInElementAdvancedNegative() {
        Element element = driver.findElement(By.id("pageheader"));
        Assert.assertFalse(element.syncTextInElement("negative", 2, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncTextMatchesInElementBasic() {
        Element element = driver.findElement(By.id("pageheader"));
        Assert.assertTrue(element.syncTextMatchesInElement("(.*test page)"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true, expectedExceptions = TextInElementNotPresentException.class)
    public void syncTextMatchesInElementBasicNegative() {
        Element element = driver.findElement(By.id("pageheader"));
        element.syncTextMatchesInElement("(.*tst pge)");
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncTextMatchesInElementAdvanced() {
        Element element = driver.findElement(By.id("pageheader"));
        Assert.assertTrue(element.syncTextMatchesInElement("(.*test page)", 5, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncTextMatchesInElementAdvancedNegative() {
        Element element = driver.findElement(By.id("pageheader"));
        Assert.assertFalse(element.syncTextMatchesInElement("(.*tst pge)", 2, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncAttributeContainsValueBasic() {
        Element element = driver.findElement(By.xpath("//input[@value='female']"));
        Assert.assertTrue(element.syncAttributeContainsValue("type", "radio"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true, expectedExceptions = ElementAttributeValueNotMatchingException.class)
    public void syncAttributeContainsValueBasicNegative() {
        Element element = driver.findElement(By.xpath("//input[@value='female']"));
        element.syncAttributeContainsValue("type", "Radio");
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncAttributeContainsValueAdvanced() {
        Element element = driver.findElement(By.xpath("//input[@value='female']"));
        Assert.assertTrue(element.syncAttributeContainsValue("type", "radio", 5, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncAttributeContainsValueAdvancedNegative() {
        Element element = driver.findElement(By.xpath("//input[@value='female']"));
        Assert.assertFalse(element.syncAttributeContainsValue("type", "Rao", 2, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncAttributeMatchesValueBasic() {
        Element element = driver.findElement(By.xpath("//input[@value='female']"));
        Assert.assertTrue(element.syncAttributeMatchesValue("type", "(.*adi.*)"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = "reload", alwaysRun = true, expectedExceptions = ElementAttributeValueNotMatchingException.class)
    public void syncAttributeMatchesValueBasicNegative() {
        Element element = driver.findElement(By.xpath("//input[@value='female']"));
        element.syncAttributeMatchesValue("type", "(.*adi)");
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncAttributeMatchesValueAdvanced() {
        Element element = driver.findElement(By.xpath("//input[@value='female']"));
        Assert.assertTrue(element.syncAttributeMatchesValue("type", "(.*adi.*)"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncAttributeMatchesValueAdvancedNegative() {
        Element element = driver.findElement(By.xpath("//input[@value='female']"));
        Assert.assertFalse(element.syncAttributeMatchesValue("type", "(.*adi)", 2, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = { "reload", "highlight" }, alwaysRun = true)
    public void syncCssPropertyContainsValueBasic() {
        WebElement element = driver.findElement(By.id("buttonForText1"));
        Assert.assertTrue(element.syncCssPropertyContainsValue("display", "inline"));

    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = { "reload", "highlight" }, alwaysRun = true, expectedExceptions = ElementCssValueNotMatchingException.class)
    public void syncCssPropertyContainsValueBasicNegative() {
        WebElement element = driver.findElement(By.id("buttonForText1"));
        element.syncCssPropertyContainsValue("display", "Inline");
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = { "reload", "highlight" }, alwaysRun = true)
    public void syncCssPropertyContainsValueAdvanced() {
        WebElement element = driver.findElement(By.id("buttonForText1"));
        Assert.assertTrue(element.syncCssPropertyContainsValue("display", "inline", 5, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = "reload", alwaysRun = true)
    public void syncCssPropertyContainsValueAdvancedNegative() {
        WebElement element = driver.findElement(By.id("buttonForText1"));
        Assert.assertFalse(element.syncCssPropertyContainsValue("display", "Inline", 2, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = { "reload", "highlight" }, alwaysRun = true)
    public void syncCssPropertyMatchesValueBasic() {
        WebElement element = driver.findElement(By.id("buttonForText1"));
        Assert.assertTrue(element.syncCssPropertyMatchesValue("display", "(.*inline.*)"));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "interfaces", "element" }, dependsOnMethods = { "reload", "highlight" }, alwaysRun = true, expectedExceptions = ElementCssValueNotMatchingException.class)
    public void syncCssPropertyMatchesValueBasicNegative() {
        WebElement element = driver.findElement(By.id("buttonForText1"));
        element.syncCssPropertyMatchesValue("display", "(.*Inline.*)");
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = { "reload", "highlight" }, alwaysRun = true)
    public void syncCssPropertyMatchesValueAdvanced() {
        WebElement element = driver.findElement(By.id("buttonForText1"));
        Assert.assertTrue(element.syncCssPropertyMatchesValue("display", "(.*inline.*)", 2, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression", "element" }, dependsOnMethods = { "reload", "highlight" }, alwaysRun = true)
    public void syncCssPropertyMatchesValueAdvancedNegative() {
        WebElement element = driver.findElement(By.id("buttonForText1"));
        Assert.assertFalse(element.syncCssPropertyMatchesValue("display", "(.*Inline-Block.*)", 2, false));
    }

    @Feature("Element Interfaces")
    @Story("Element")

    @Test(groups = { "regression" }, dependsOnGroups = "element", alwaysRun = true)
    public void syncInFrame() {
        driver.get("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/utils/frameHandler.html");
        driver.findElement(By.id("button_frame1")).syncInFrame();
    }

}
