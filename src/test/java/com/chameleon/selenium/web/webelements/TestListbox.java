package com.chameleon.selenium.web.webelements;

import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.elements.Listbox;
import com.chameleon.selenium.exceptions.OptionNotInListboxException;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.WebException;
import com.chameleon.selenium.web.elements.WebButton;
import com.chameleon.selenium.web.elements.WebListbox;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestListbox extends WebBaseTest {
    @FindBy(id = "wrongID")
    Listbox badSelect;
    @FindBy(id = "singleSelect")
    Listbox listbox;

    ExtendedWebDriver driver = null;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        setApplicationUnderTest("Test Site");
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/listbox.html");
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" })
    public void constructorWithElement() {
        driver = testStart("TestListbox");
        Assert.assertNotNull((new WebListbox(driver, (By.id("singleSelect")))));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "constructorWithElement")
    public void isMultiple() {
        Listbox listbox = driver.findListbox(By.id("multiSelect"));
        Assert.assertTrue(listbox.isMultiple());
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "constructorWithElement")
    public void select() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        listbox.select("Sports");
        Assert.assertTrue(listbox.getFirstSelectedOption().getText().equals("Sports"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "constructorWithElement")
    public void selectFromMultiple() {
        Listbox listbox = driver.findListbox(By.id("multiSelectFalse"));
        listbox.select("Tennis");
        Assert.assertTrue(listbox.getFirstSelectedOption().getText().equals("Tennis"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "selectFromMultiple")
    public void selectValueFromMultiple() {
        Listbox listbox = driver.findListbox(By.id("multiSelectFalse"));
        listbox.selectValue("Badminton");
        Assert.assertTrue(listbox.getFirstSelectedOption().getText().equals("Badminton"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "select")
    public void selectNoText() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        listbox.select("");
        Assert.assertTrue(listbox.getAttribute("value").equals("Sports"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "select", expectedExceptions = OptionNotInListboxException.class)
    public void selectNegative() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        listbox.select("text");
        Assert.assertTrue(false);
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "constructorWithElement")
    public void selectValue() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        listbox.selectValue("Sports");
        Assert.assertTrue(listbox.getFirstSelectedOption().getText().equals("Sports"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "select")
    public void selectValueNoText() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        listbox.selectValue("");
        Assert.assertTrue(listbox.getAttribute("value").equals("Sports"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "select", expectedExceptions = OptionNotInListboxException.class)
    public void selectValueNegative() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        listbox.selectValue("text");
        Assert.assertTrue(false);
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "select")
    public void getAllOptions() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        Assert.assertTrue(listbox.getOptions().get(0).getText().equals("Other"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "select")
    public void getAllSelectedOptions() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        Assert.assertTrue(listbox.getAllSelectedOptions().get(0).getText().equals("Sports"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "getAllSelectedOptions")
    public void isSelected() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        Assert.assertTrue(listbox.isSelected("Sports"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "isSelected")
    public void deselectByVisibleText() {
        Listbox listbox = driver.findListbox(By.id("multiSelect"));
        listbox.select("Baseball");
        Assert.assertTrue(listbox.isSelected("Baseball"));
        listbox.deselectByVisibleText("Baseball");
        Assert.assertFalse(listbox.isSelected("Baseball"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "constructorWithElement", expectedExceptions = WebException.class)
    public void deselectByVisibleTextNonMulti() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        listbox.deselectByVisibleText("Soccer");
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "deselectByVisibleText")
    public void deselectAll() {
        Listbox listbox = driver.findListbox(By.id("multiSelect"));
        listbox.select("Basketball");
        listbox.select("Soccer");
        listbox.deselectAll();
        Assert.assertNull(listbox.getFirstSelectedOption());
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox" }, dependsOnMethods = "constructorWithElement", expectedExceptions = WebException.class)
    public void deselectAllNonMulti() {
        Listbox listbox = driver.findListbox(By.id("singleSelect"));
        listbox.deselectAll();
    }

    @Feature("Element Interfaces")
    @Story("Listbox")
    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "deselectAllNonMulti")
    public void unorderedList() {
        DriverManager.setDriver(driver);
        WebButton listButton = driver.findButton(By.id("menu1"));
        listButton.click();

        Listbox listbox = driver.findListbox(By.className("dropdown-menu"));
        // listbox.syncVisible();
        listbox.select("HTML");
        Assert.assertTrue(listButton.getText().equals("HTML"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "unorderedList")
    public void overrideOptionTag() {
        WebButton listButton = driver.findButton(By.id("menu1"));
        listButton.click();

        Listbox listbox = driver.findListbox(By.className("dropdown-menu"));
        listbox.syncVisible();
        listbox.overrideOptionTag("a");
        listbox.select("Mercury");
        Assert.assertTrue(listButton.getText().equals("Mercury"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "overrideOptionTag")
    public void overrideClickableTag() {
        WebButton listButton = driver.findButton(By.id("menu1"));
        listButton.click();

        Listbox listbox = driver.findListbox(By.className("dropdown-menu"));
        listbox.syncVisible();
        listbox.overrideClickableTag("a");
        listbox.select("Jupiter");
        Assert.assertTrue(listButton.getText().equals("Jupiter"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "constructorWithElement", expectedExceptions = WebException.class)
    public void overrideOptionTagNull() {
        Listbox listbox = driver.findListbox(By.className("dropdown-menu"));
        listbox.overrideOptionTag("");
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "constructorWithElement", expectedExceptions = WebException.class)
    public void overrideClickableTagNull() {
        Listbox listbox = driver.findListbox(By.className("dropdown-menu"));
        listbox.overrideClickableTag("");
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "constructorWithElement")
    public void getOptionValues() {
        String options = driver.findListbox(By.id("singleSelect")).getOptionValues().stream().collect(Collectors.joining(" "));
        Assert.assertTrue(options.contains("Other"));
        Assert.assertTrue(options.contains("Music"));
        Assert.assertTrue(options.contains("Movie"));
        Assert.assertTrue(options.contains("Science"));
        Assert.assertTrue(options.contains("Sports"));
        Assert.assertTrue(options.contains("Politics"));
    }

    @Feature("Element Interfaces")
    @Story("Listbox")

    @Test(groups = { "regression", "interfaces", "listbox", "mustard" }, dependsOnMethods = "constructorWithElement")
    public void getOptionTextValues() {
        String options = driver.findListbox(By.id("singleSelect")).getOptionTextValues().stream().collect(Collectors.joining(" "));
        Assert.assertTrue(options.contains("Other"));
        Assert.assertTrue(options.contains("Music"));
        Assert.assertTrue(options.contains("Movie"));
        Assert.assertTrue(options.contains("Science"));
        Assert.assertTrue(options.contains("Sports"));
        Assert.assertTrue(options.contains("Politics"));
    }

}
