package com.chameleon.selenium.web.webelements;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.elements.Textbox;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.elements.WebTextbox;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestTextbox extends WebBaseTest {
    ExtendedWebDriver driver = null;

    @BeforeClass(groups = { "regression", "interfaces", "textbox", "dev" })
    public void setup() {
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/textbox.html");
    }

    @BeforeMethod(alwaysRun = true)
    public void setDriver() {
        DriverManager.setDriver(driver);
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" })
    public void constructorWithElement() {
        driver = testStart("TestTextbox");
        Assert.assertNotNull((new WebTextbox(driver, (By.id("text1")))));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "constructorWithElement")
    public void constructorWithElementAndDriver() {
        Assert.assertNotNull(new WebTextbox(driver, By.id("text1")));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "constructorWithElement")
    public void getText() {
        Textbox textbox = driver.findTextbox(By.id("text1"));
        Assert.assertTrue(textbox.getText().equals("Testing methods"));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "setNoText")
    public void set() {
        Textbox textbox = driver.findTextbox(By.id("text1"));
        textbox.set("set");
        Assert.assertTrue(textbox.getAttribute("value").equals("set"));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "getText")
    public void setNoText() {
        Textbox textbox = driver.findTextbox(By.id("text1"));
        textbox.set("");
        Assert.assertTrue(textbox.getAttribute("value").equals("Testing methods"));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "set")
    public void setNegative() {
        Textbox textbox = driver.findTextbox(By.name("lname"));
        boolean valid = false;
        try {
            textbox.set("text");
        } catch (RuntimeException rte) {
            valid = true;
        }
        Assert.assertTrue(valid);
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "set")
    public void scrollAndSet() {
        DriverManager.setDriver(driver);
        WebTextbox textbox = driver.findTextbox(By.id("text1"));
        textbox.scrollAndSet("setScrollIntoView");
        Assert.assertTrue(textbox.getAttribute("value").equals("setScrollIntoView"));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "scrollAndSet")
    public void scrollAndSetNoText() {
        WebTextbox textbox = driver.findTextbox(By.id("text1"));
        textbox.scrollAndSet("");
        Assert.assertTrue(textbox.getAttribute("value").equals("setScrollIntoView"));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "scrollAndSet")
    public void scrollAndSetNegative() {
        WebTextbox textbox = driver.findTextbox(By.name("lname"));
        boolean valid = false;
        try {
            textbox.scrollAndSet("text");
        } catch (RuntimeException rte) {
            valid = true;
        }
        Assert.assertTrue(valid);
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "scrollAndSetNoText")
    public void clear() {
        Textbox textbox = driver.findTextbox(By.id("text1"));
        textbox.clear();
        Assert.assertTrue(textbox.getAttribute("value").equals(""));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "clear")
    public void clearNegative() {
        Textbox textbox = driver.findTextbox(By.name("lname"));
        boolean valid = false;
        try {
            textbox.clear();
        } catch (RuntimeException rte) {
            valid = true;
        }
        Assert.assertTrue(valid);
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "clear")
    public void safeSet() {
        WebTextbox textbox = driver.findTextbox(By.id("text1"));
        textbox.safeSet("safeSet");
        Assert.assertTrue(textbox.getAttribute("value").contains("safeSet"));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "safeSet")
    public void safeSetNoText() {
        WebTextbox textbox = driver.findTextbox(By.id("text1"));
        textbox.safeSet("");
        Assert.assertTrue(textbox.getAttribute("value").contains("safeSet"));
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "safeSet")
    public void safeSetNegative() {
        WebTextbox textbox = driver.findTextbox(By.name("page"));
        boolean valid = false;
        try {
            textbox.safeSet("text");
        } catch (RuntimeException rte) {
            valid = true;
        }
        Assert.assertTrue(valid);
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "safeSet")
    public void setSecure() {
        Textbox textbox = driver.findTextbox(By.id("text1"));
        textbox.setSecure("aWg4sQgcN5pWzW8ijVVzVD+qXlATFfNvWZhkF9hniK81edEozzkKjeTplYLZTf1d");
        Assert.assertTrue(textbox.getAttribute("value").contains("safeSetsetSecure"));
        textbox.clear();
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "setSecure")
    public void setSecureNoText() {
        Textbox textbox = driver.findTextbox(By.id("text1"));
        textbox.setSecure("");
        Assert.assertTrue(textbox.getAttribute("value").contains(""));
        textbox.clear();
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "setSecureNoText")
    public void setSecureNegative() {
        Textbox textbox = driver.findTextbox(By.name("lname"));
        boolean valid = false;
        try {
            textbox.setSecure("tex");
        } catch (RuntimeException rte) {
            valid = true;
        }
        Assert.assertTrue(valid);
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "setSecure")
    public void safeSetSecure() {
        WebTextbox textbox = driver.findTextbox(By.id("text1"));
        textbox.clear();
        textbox.safeSetSecure("ZoJSmSxWx3ZJtx769BxHv+g98jh6zMyPehaCR7+nkdM=");
        Assert.assertTrue(textbox.getAttribute("value").contains("setSecure"));
        textbox.clear();
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "safeSetSecure")
    public void safeSetSecureNoText() {
        WebTextbox textbox = driver.findTextbox(By.id("text1"));
        textbox.safeSetSecure("");
        Assert.assertTrue(textbox.getAttribute("value").contains(""));
        textbox.clear();
    }

    @Feature("Element Interfaces")
    @Story("Textbox")

    @Test(groups = { "regression", "interfaces", "textbox" }, dependsOnMethods = "safeSetSecure")
    public void safeSetSecureNegative() {
        WebTextbox textbox = driver.findTextbox(By.name("lname"));
        boolean valid = false;
        try {
            textbox.safeSetSecure("tex");
        } catch (RuntimeException rte) {
            valid = true;
        }
        Assert.assertTrue(valid);
    }
}
