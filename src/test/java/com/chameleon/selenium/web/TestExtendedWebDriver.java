package com.chameleon.selenium.web;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.chameleon.AutomationException;
import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.DriverManagerFactory;
import com.chameleon.selenium.DriverOptionsManager;
import com.chameleon.selenium.DriverProperties;
import com.chameleon.selenium.DriverType;
import com.chameleon.selenium.elements.Button;
import com.chameleon.selenium.elements.Checkbox;
import com.chameleon.selenium.elements.Element;
import com.chameleon.selenium.elements.Label;
import com.chameleon.selenium.elements.Link;
import com.chameleon.selenium.elements.Listbox;
import com.chameleon.selenium.elements.RadioGroup;
import com.chameleon.selenium.elements.Table;
import com.chameleon.selenium.elements.Textbox;
import com.chameleon.selenium.web.by.angular.ByNG;
import com.chameleon.selenium.web.elements.WebButton;
import com.chameleon.selenium.web.elements.WebCheckbox;
import com.chameleon.selenium.web.elements.WebLabel;
import com.chameleon.selenium.web.elements.WebLink;
import com.chameleon.selenium.web.elements.WebTextbox;
import com.chameleon.utils.Base64Coder;
import com.chameleon.utils.Constants;
import com.chameleon.utils.Sleeper;
import com.chameleon.utils.exception.KeyExistsException;
import com.chameleon.utils.exception.NoKeyFoundException;
import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.saucerest.SauceREST;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestExtendedWebDriver extends WebBaseTest {

    static int elementTimeout = DriverProperties.getDriverTimeout();
    static int pageTimeout = DriverProperties.getPageTimeout();
    static int globalTimeout = DriverProperties.getDriverTimeout();

    protected ResourceBundle appURLRepository = ResourceBundle.getBundle(Constants.ENVIRONMENT_URL_PATH);
    protected SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(
            Base64Coder.decodeString(appURLRepository.getString("SAUCELABS_USERNAME")),
            Base64Coder.decodeString(appURLRepository.getString("SAUCELABS_KEY")));
    // DesiredCapabilities caps = null;
    ExtendedWebDriver driver = null;
    File file = null;
    String runLocation = "";
    String browserUnderTest = "";
    String browserVersion = "";
    String operatingSystem = "";
    String application = "";

    @BeforeClass(groups = { "regression", "utils", "ExtendedWebdriver" })
    @Parameters({ "runLocation", "browserUnderTest", "browserVersion", "operatingSystem", "application" })
    public void setup(@Optional String runLocation, String browserUnderTest, String browserVersion, String operatingSystem, String application) {
        // setReportToMustard(false);

        if (browserUnderTest.equalsIgnoreCase("jenkinsParameter")) {
            this.browserUnderTest = System.getProperty("jenkinsBrowser").trim();
        } else {
            this.browserUnderTest = browserUnderTest;
        }

        if (browserVersion.equalsIgnoreCase("jenkinsParameter")) {
            browserVersion = System.getProperty("jenkinsBrowserVersion").trim();
        } else {
            this.browserVersion = browserVersion;
        }

        if (operatingSystem.equalsIgnoreCase("jenkinsParameter")) {
            operatingSystem = System.getProperty("jenkinsOperatingSystem").trim();
        } else {
            this.operatingSystem = operatingSystem;
        }

        if (runLocation.equalsIgnoreCase("jenkinsParameter")) {
            runLocation = System.getProperty("jenkinsRunLocation").trim();
        } else {
            this.runLocation = runLocation;
        }
        if (application.equalsIgnoreCase("jenkinsParameter")) {
            application = System.getProperty("jenkinsRunLocation").trim();
        } else {
            this.application = application;
        }

        setRunLocation(runLocation);
        if (runLocation.toLowerCase().equals("local")) {

            if (DriverType.HTML.equals(DriverType.fromString(this.browserUnderTest))) {
                DriverOptionsManager options = new DriverOptionsManager();
                options.getFirefoxOptions().setHeadless(true);
                setBrowserUnderTest("firefox");
                DriverManagerFactory.getManager(DriverType.fromString("firefox"), options).initalizeDriver();
            } else {
                DriverManagerFactory.getManager(DriverType.fromString(this.browserUnderTest)).initalizeDriver();
            }
            driver = (ExtendedWebDriver) DriverManager.getDriver();
        } else {
            try {
                DriverManagerFactory.getManager(DriverType.fromString(browserUnderTest)).initalizeDriver(new URL(getRemoteURL()));
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            driver = (ExtendedWebDriver) DriverManager.getDriver();
        }

        driver.launchApplication(application);
        driver.get("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/testsite.html");
        Sleeper.sleep(5);
        elementTimeout = DriverProperties.getDriverTimeout();
        pageTimeout = DriverProperties.getPageTimeout();
        globalTimeout = DriverProperties.getDriverTimeout();

        driver.setElementTimeout(elementTimeout);
        driver.setPageTimeout(pageTimeout);
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    private void endSauceTest(int result) {
        Map<String, Object> updates = new HashMap<String, Object>();
        updates.put("name", "TestExtendedWebDriver");

        if (result == ITestResult.FAILURE) {
            updates.put("passed", false);
        } else {
            updates.put("passed", true);
        }

        SauceREST client = new SauceREST(authentication.getUsername(), authentication.getAccessKey());
        client.updateJobInfo(driver.getSessionId().toString(), updates);

    }

    @AfterClass(groups = { "regression", "utils", "ExtendedWebdriver" })
    public void close(ITestContext testResults) {
        if (runLocation.equalsIgnoreCase("sauce")) {
            if (testResults.getFailedTests().size() == 0) {
                endSauceTest(ITestResult.SUCCESS);
            } else {
                endSauceTest(ITestResult.FAILURE);
            }
        }
        driver.quit();
        DriverManager.quitDriver();
        DriverManager.stopService();
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" })
    public void setExtendedWebDriver() {
        ExtendedWebDriver ExtendedWebDriver = new ExtendedWebDriver();
        ExtendedWebDriver.setDriver(ExtendedWebDriver);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" })
    public void getPageTimeout() {
        Assert.assertTrue(driver.getPageTimeout() == pageTimeout);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "getPageTimeout")
    public void setPageTimeout() {
        if (this.browserUnderTest.toLowerCase().contains("safari") || driver.toString().contains("safari")) {
            throw new SkipException("Test not valid for SafariDriver");
        }
        driver.setPageTimeout(15);
        Assert.assertTrue(driver.getPageTimeout() == 15);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" })
    public void getElementTimeout() {
        Assert.assertTrue(driver.getElementTimeout() == elementTimeout);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "getElementTimeout")
    public void setElementTimeout() {
        driver.setElementTimeout(15);
        Assert.assertTrue(driver.getElementTimeout() == 15);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "executeJavaScript")
    public void getScriptTimeout() {
        Assert.assertTrue(driver.getScriptTimeout() == globalTimeout);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "getScriptTimeout")
    public void setScriptTimeout() {
        driver.setScriptTimeout(15);
        Assert.assertTrue(driver.getScriptTimeout() == 15);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "setScriptTimeout")
    public void testGetDriver() {
        Assert.assertNotNull(driver.getWebDriver());
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" })
    public void executeJavaScript() {
        driver.findTextbox(By.id("FirstName")).elementWired();

        WebElement element = (WebElement) driver.executeJavaScript("return document.getElementById('FirstName')");
        Assert.assertNotNull(element);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "testGetDriver")
    public void findElements() {
        List<WebElement> elements = driver.findElements(By.tagName("input"));
        Assert.assertTrue(elements.size() > 0);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findElements")
    public void findWebElements() {
        List<WebElement> elements = driver.findElements(By.tagName("input"));
        Assert.assertTrue(elements.size() > 0);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findElements")
    public void findTextboxes() {
        List<WebTextbox> elements = driver.findTextboxes(By.tagName("input"));
        Assert.assertTrue(elements.size() > 0);
        Assert.assertTrue(elements.get(0) instanceof WebTextbox);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "testGetDriver")
    public void findButton() {
        Button button = driver.findButton(By.id("Add"));
        Assert.assertNotNull(button);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findElements")
    public void findButtons() {
        List<Button> elements = driver.findButtons(By.tagName("input"));
        Assert.assertTrue(elements.size() > 0);
        Assert.assertTrue(elements.get(0) instanceof WebButton);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findElements")
    public void findCheckboxes() {
        List<Checkbox> elements = driver.findCheckboxes(By.tagName("input"));
        Assert.assertTrue(elements.size() > 0);
        Assert.assertTrue(elements.get(0) instanceof WebCheckbox);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findElements")
    public void findLabels() {
        List<Label> elements = driver.findLabels(By.tagName("input"));
        Assert.assertTrue(elements.size() > 0);
        Assert.assertTrue(elements.get(0) instanceof WebLabel);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findElements")
    public void findLinks() {
        List<Link> elements = driver.findLinks(By.tagName("input"));
        Assert.assertTrue(elements.size() > 0);
        Assert.assertTrue(elements.get(0) instanceof WebLink);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findWebElements")
    public void findElement() {
        Element element = driver.findElement(By.id("FirstName"));
        Assert.assertNotNull(element);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findElement")
    public void findListbox() {
        driver.setElementTimeout(3);
        Listbox listbox = driver.findListbox(By.id("Category"));
        Assert.assertNotNull(listbox);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findListbox")
    public void findTextbox() {
        Textbox textbox = driver.findTextbox(By.id("FirstName"));
        Assert.assertNotNull(textbox);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findTextbox")
    public void findWebtable() {
        Table webtable = driver.findTable(By.id("VIPs"));
        Assert.assertNotNull(webtable);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findWebtable")
    public void findRadioGroup() {
        driver.get("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/radioGroup.html");
        RadioGroup radioGroup = driver.findRadioGroup(By.id("Content"));
        Assert.assertNotNull(radioGroup);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findRadioGroup")
    public void findCheckbox() {
        driver.get("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/checkbox.html");
        Checkbox checkbox = driver.findCheckbox(By.name("checkbox"));
        Assert.assertNotNull(checkbox);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findCheckbox")
    public void findLabel() {
        driver.get("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/label.html");
        Label label = driver.findLabel(By.xpath("//*[@id='radioForm']/label[1]"));
        Assert.assertNotNull(label);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findLabel")
    public void findLink() {
        driver.get("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/link.html");
        Link link = driver.findLink(By.xpath("//a[@href='testLinks.html']"));
        Assert.assertNotNull(link);
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findLink")
    public void executeAsyncJavaScript() {
        Sleeper.sleep(2);
        driver.get("http://cafetownsend-angular-rails.herokuapp.com/login");
        Sleeper.sleep(3);
        driver.executeAsyncJavaScript(
                "var callback = arguments[arguments.length - 1];angular.element(document.body).injector().get('$browser').notifyWhenNoOutstandingRequests(callback);");
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "executeAsyncJavaScript")
    public void getCurrentUrl() {
        Assert.assertTrue(driver.getCurrentUrl().contains("cafetownsend-angular-rails.herokuapp.com"));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "executeAsyncJavaScript")
    public void getTitle() {
        Assert.assertTrue(driver.getTitle().contains("CafeTownsend-AngularJS-Rails"));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "executeAsyncJavaScript")
    public void findNGModel() {
        Assert.assertNotNull(driver.findTextbox(ByNG.model("user.name")));
        driver.findTextbox(ByNG.model("user.name")).set("Luke");
        driver.findTextbox(ByNG.model("user.password")).set("Skywalker");
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findNGModel")
    public void findNGController() {
        Assert.assertNotNull(driver.findElement(ByNG.controller("HeaderController")));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findNGController")
    public void findNGRepeater() {
        Sleeper.sleep(2);
        Assert.assertNotNull(driver.findElement(ByNG.repeater("employee in employees")));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" })
    public void getDataWarehouse() {
        Assert.assertNotNull(driver.data());
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "getDataWarehouse")
    public void addData() {
        driver.data().add("username", "Admin");
        Assert.assertNotNull(driver.data().get("username"));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "addData")
    public void getValidData() {
        Assert.assertNotNull(driver.data().get("username"));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, expectedExceptions = NoKeyFoundException.class)
    public void getInvalidData() {
        driver.data().get("password");
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils",
            "ExtendedWebdriver" }, dependsOnMethods = "addData", expectedExceptions = KeyExistsException.class)
    public void addToExistingKey() {
        driver.data().add("username", "Employee");
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "addData")
    public void updateExistingKey() {
        driver.data().update("username", "Manager");
        Assert.assertEquals("Manager", driver.data().get("username"));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, expectedExceptions = NoKeyFoundException.class)
    public void updateNoKeyFound() {
        driver.data().update("password", "ExtendedWeb123");
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "findNGRepeater")
    public void isAngularComplete() {
        DriverManager.setDriver(driver);
        Assert.assertNotNull(driver.page().isAngularComplete());
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "isAngularComplete")
    public void isDomComplete() {
        DriverManager.setDriver(driver);
        Assert.assertNotNull(driver.page().isDomComplete());
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "isDomComplete")
    public void isDomCompleteWithTimeout() {
        DriverManager.setDriver(driver);
        Assert.assertNotNull(driver.page().isDomComplete(3));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "isDomCompleteWithTimeout")
    public void isDomInteractive() {
        DriverManager.setDriver(driver);
        Assert.assertNotNull(driver.page().isDomInteractive());
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "isDomInteractive")
    public void isDomInteractiveWithTimeout() {
        DriverManager.setDriver(driver);
        Assert.assertNotNull(driver.page().isDomInteractive(3));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "isDomInteractiveWithTimeout")
    public void isJQueryComplete() {
        driver.get("http://www.kariyer.net/");
        DriverManager.setDriver(driver);
        Assert.assertNotNull(driver.page().isJQueryComplete());
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "isDomInteractiveWithTimeout")
    public void isJQueryCompleteWithTimeout() {
        driver.get("http://www.kariyer.net/");
        DriverManager.setDriver(driver);
        driver.page().isDomComplete();
        Assert.assertNotNull(driver.page().isJQueryComplete(15));
    }

    @Feature("Utilities")
    @Story("ExtendedWebDriver")

    @Test(groups = { "regression", "utils", "ExtendedWebdriver" }, dependsOnMethods = "isJQueryComplete", expectedExceptions = AutomationException.class)
    public void isJQueryCompleteOnNonJQueryPage() {
        driver.get("http://www.google.com/");
        DriverManager.setDriver(driver);
        driver.page().isDomComplete();
        Assert.assertNotNull(driver.page().isJQueryComplete());
    }

}
