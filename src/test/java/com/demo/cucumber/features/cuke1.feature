@cucumberdemo @cuke
Feature: Chameleon Demo Cuke

  Background: Launch the browser
    Given I have launched the browser
    
  Scenario: Navigate to Etsy
    When I navigate to "Etsy"
    Then the "Etsy" landing page is displayed
  
  Scenario: Navigate to Google
    When I navigate to "Google"
    Then the "Google" landing page is displayed